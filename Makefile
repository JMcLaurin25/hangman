CFLAGS+=-std=c11 -Werror -Wno-deprecated -pedantic -Wall -Wextra -pedantic -Wwrite-strings -fstack-usage -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

TARGET=hangman
OBJS= hangman.o data_in.o data_out.o data_procs.o screen_print.o

$(TARGET): $(OBJS)

.PHONY: clean debug

clean:
	rm $(TARGET) $(OBJS) *.su

debug: CFLAGS+=-g
debug: $(TARGET)
