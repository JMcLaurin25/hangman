#ifndef DATA_PROCS_H
#define DATA_PROCS_H

void check_answer(char *, int *, char, int *);
int win_lose(int *, char *);
void mask_reset(int *);
void set_zero(char *, int *);
void store_stats(int *, int *, int);

#endif
