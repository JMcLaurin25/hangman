/*Primary/Main file*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data_in.h"
#include "data_out.h"
#include "data_procs.h"
#include "screen_print.h"

int main(int argc, char *argv[])
{	
	char *home_dir = getenv("HOME");
	char word_list[32] = "", stats_dir[32] = "";
	int length_home = strlen(home_dir), stat_values[6] = {0};

	strncat(stats_dir, home_dir, length_home);
	strncat(stats_dir, "/.hangman", 9); //Creates the address to the default statistics file - .hangman

	stats_in(stats_dir, stat_values); // Read and set the statistics from the .hangman document

	switch(argc) {
		case 1:// Uses the default '.words' file for dictionary
			strncat(word_list, home_dir, length_home);
			strncat(word_list, "/.words", 7);
			break;
		case 2:// Uses the users file for dictionary
			strncpy(word_list, argv[1], strlen(argv[1]) + 1);
			break;
		default:
			break;
	}
		print_welcome();
		int program_loop = 0, game_loop, counts[2] = {0};
		static int mask[26];

	while(!program_loop) {
		char secret_word[32], userguess[2];
		file_in(word_list, secret_word); //Reads the dictionary and sets the secret word

		print_selection(word_list);
		
		int *mask_word = malloc(strlen(secret_word) * sizeof(*mask_word));//Variable space allotment for the random word
		set_zero(secret_word, mask_word);//Reset the mask to {0}

		if (menu_in(stats_dir, stat_values, mask_word) == 2) {
			mask_reset(mask);
			game_loop = 0;
			counts[0] = 0;//Guesses
			counts[1] = 0;//Misses
			while (!game_loop) {
				print_game(mask, mask_word, secret_word, counts);

				letter_in(mask, userguess);

				check_answer(secret_word, mask_word, userguess[0], counts);

				game_loop = win_lose(mask_word, secret_word);

				if (game_loop == 2) {
					print_win(secret_word);
					store_stats(counts, stat_values, 0);
					stats_out(stats_dir, stat_values); //Save stats
					print_stats(stat_values);
					break;
				} else if (counts[1] == 6) { //6 misses hang the man
					print_lose();
					store_stats(counts, stat_values, 1);
					stats_out(stats_dir, stat_values);
					print_stats(stat_values);
					break;
				}

			}
		} else {
			print_stats(stat_values);
		}
		free(mask_word);
	}
}
