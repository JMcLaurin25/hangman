/*For all file output functions*/
#include <stdio.h>

void stats_out(char *stats_address, int *values)
{
	int index;

	FILE *game_stats;
	game_stats = fopen(stats_address, "w");
	if ( game_stats == NULL) { //If file creation failed
		printf("Could not create statistics file.\n");
	} else {

		for (index = 0; index < 6; index++) {
			fprintf(game_stats, "%d\n", values[index]); //Write stats to .hangman
		}

		fclose(game_stats);
	}

}
