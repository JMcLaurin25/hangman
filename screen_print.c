/*For all printing functions*/
#include <string.h>
#include <stdio.h>

void border(void);
void lose(void);
void winner(char *);
void miss5(void);
void miss4(void);
void miss3(void);
void miss2(void);
void miss1(void);
void new(void);
void indent(void);

void print_alphabet(int *mask)
{
	int index;
	printf("\n   ");
	for (index = 'a'; index <= 'z'; index++) {
		if (mask[index - 'a'] == 1) { //If letter in word is not alphabetic
			printf("-");

			if (mask[index - 'a' + 1] != '\n') { //Space between letters
				printf(" ");
			}

		} else {
			printf("%c", index);
			if (mask[index - 'a' + 1] != '\n') { //prints the letter and the space between
				printf(" ");
			}
		}
	}
	printf("\n");
	border();
}

void print_word(int *mask, char *word)
{
	int index, length = strlen(word);
	for (index = 0; index < length; index++) { //If mask = 1, print the letter
		if ((mask[index] == 1) || (!(word[index] >= 'a' && word[index] <= 'z') && !(word[index] >= 'A' && word[index] <= 'Z'))) {
			printf("%c", word[index]);
			printf(" ");
		} else {
			printf("_");
			printf(" ");
		}
	}
	printf("\n");
}

void print_welcome(void)
{
	border();
	printf("\n%38s\n", "Welcome to Hangman");
}

void print_exit(void)
{
	border();
	printf("\n%36s\n", "Exiting Hangman");
	border();
}

void print_selection(char *wdlist)
{
	border();
	printf("\n\t%s%17s%10s\n", "(1) Play Game", "(2) Statistics", "(3) EXIT");
	printf("\n\t%s %s\n", "Word file used:", wdlist);
	border();
	printf("\n Selection: ");
}

void print_game(int *mask, int *mask_word, char *answer, int *counts)
{
	border();
	print_alphabet(mask);
	printf("\n\t Guess #%d\t Misses: %d\n", counts[0], counts[1]);

	switch (counts[1]) {
		case 0:
			new();
			break;
		case 1:
			miss1();
			break;
		case 2:
			miss2();
			break;
		case 3:
			miss3();
			break;
		case 4:
			miss4();
			break;
		case 5:
			miss5();
			break;
		default:
			break;
	}

	printf("\n\t Word: ");
	print_word(mask_word, answer);

	border();
	printf("\n Guess a letter: ");
}

void print_win(char *answer)
{
	border();
	printf("\n\t\t\tWINNER!\n");
	winner(answer);
}

void print_lose(void)
{
	printf("\n\t\t\tLOSER!\n");
	lose();
}

void print_stats(int *sums)
{
	double average_score = 0;
	char win[8], lose[8];

	if (sums[1] > 1) { //Plurality Check
		strncpy(win, "Win", sizeof(win));
	} else {
		strncpy(win, "Wins", sizeof(win));
	}

	if (sums[2] > 1) { //Plurality Check
		strncpy(lose, "Loss", sizeof(win));
	} else {
		strncpy(lose, "Losses", sizeof(win));
	}

	if (sums[0] > 0) {
		average_score = (1.0 * sums[4] ) / (1.0 * sums[0]);
	} else {
		average_score = sums[4];
	}

	printf("\n%38s\n", "--STATISTICS--");
	printf("%23s: %3d %16s: %3.1f\n", "Games Played", sums[0], "Average Score", average_score);

	printf("%23s: %3d %16s: %3d\n", "Guesses", sums[5], "Win guesses", sums[3]);

	printf("%23s: %3d %16s: %3d\n\n", win, sums[1], lose, sums[2]);

}

//Local functions

void border(void)
{
	printf(" ");

	for (int i = 0; i < 56; i++)
		printf("_");

	printf("\n");
}

void indent(void)
{
	printf("\t\t");
}

void winner(char *answer)
{
	indent();
	printf("    _______\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("   \\0/    |\tYou found\n");
	indent();
	printf("    |     |\t'%s'\n", answer);
	indent();
	printf("    |     |\n");
	indent();
	printf("___/_\\____|\n");
}

void lose(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("   /|\\    |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("   / \\    |\n");
	indent();
	printf("__________|___\n");
}

void miss5(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("   /|\\    |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("   /      |\n");
	indent();
	printf("__________|___\n");
}

void miss4(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("   /|\\    |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("          |\n");
	indent();
	printf("__________|___\n");
}

void miss3(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("   /|     |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("          |\n");
	indent();
	printf("__________|___\n");
}

void miss2(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("          |\n");
	indent();
	printf("__________|___\n");
}

void miss1(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("    0     |\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("__________|___\n");
}

void new(void)
{
	indent();
	printf("    _______\n");
	indent();
	printf("    |     |\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("          |\n");
	indent();
	printf("__________|___\n");
}
