/*For all input functions*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "data_out.h"
#include "data_procs.h"
#include "screen_print.h"

int clear_buff(FILE *);
void lower(char *);
void mask_letter(int *, char);

int menu_in(char *stats_dir, int *stat_values, int *mask_word)
{
	char selection[2];
	fgets(selection, 2, stdin);
	clear_buff(stdin);

	switch (selection[0]) {
		case '1':
			//Print next screen to play game
			printf(" -1- Selected\n");
			return 2;
		case '2':
			//Print statistics
			printf(" -2- Selected\n");
			return 4;
		case '3':
			//EXIT
			stats_out(stats_dir, stat_values);
			free(mask_word);
			exit(0);
		default:
			printf("Invalid Entry!\n");
	}
	return 0;
}

void letter_in(int *mask, char *userguess) //Input user guess
{
	fgets(userguess, 2, stdin);
	clear_buff(stdin);

	if ((userguess[0] >= 'a' && userguess[0] <= 'z' ) || (userguess[0] >= 'A' && userguess[0] <= 'Z')) { //If a valid character
		if(userguess[0] >= 'A' && userguess[0] <= 'Z') {
			lower(userguess); //Make lowercase
		}
		mask_letter(mask, userguess[0]);

	} else {
		printf("Not a valid entry.\n");
	}
}

int file_in(char *file_in, char *secret_word) //Dictionary input
{
	char cur_word[32], ch;
	int word_count = 0, word_num, line = 0;
	FILE *words;
	words = fopen(file_in, "r");

	if (words == NULL) { //File Check
		printf("\n\n\tWord file: '%s' Not found!\n\n", file_in);
		print_exit();
		exit(0);
	}

	while(!feof(words)) { //Count the words in the file
		ch = fgetc(words);
		if (ch == '\n') {
			word_count++;
		}
	}

	srand(time(NULL));//Random number
	word_num = rand() % word_count;

	fseek(words, 0, SEEK_SET);//Reset to top of file

	while (fgets(cur_word, sizeof(cur_word), words) != NULL) { //Read word from file to variable
		if (line++ == word_num) {
			strncpy(secret_word, cur_word, sizeof(cur_word));
			secret_word[strlen(secret_word) - 1] = '\0';
		}
	}

	if (!words) {
		return 0;
	} else {
		printf("\n  - Words: %d Word #: %d\n", word_count, word_num);
	}
	fclose(words);

	return 0;
}

int clear_buff(FILE *buff) //Read to end of buff
{
	int ch;

	while (((ch = fgetc(buff)) != '\n') && (ch != EOF));

	return ch;
}

void lower(char *letter) //Make lower
{
	letter[0] = letter[0] + ('a' - 'A');
}

void mask_letter(int *mask, char letter) //If found, mask it (1)
{
	int index;
	for (index = 'a'; index <= 'z'; index++) {
		if (index == letter) {
			mask[index - 'a'] = 1;
		}
	}
}

void stats_in(char *stats, int *values) //Reads in the files stats. If file not found, stats set to zero and write new file.
{
	char cur_line[8], *endptr;
	int line_count = 0, base = 10, num;

	FILE *game_stats;
	game_stats = fopen(stats, "r");

	if ( game_stats == NULL) {
		printf("Could not find statistics file. Will create new one on exit.\n");
	} else {
		while(!feof(game_stats)) {
			fgets(cur_line, sizeof(cur_line), game_stats);//Set line of the file to the index of *values
			num = strtol(cur_line, &endptr, base);

			values[line_count]+= num;
			line_count++;
		}

		fclose(game_stats);
	}
}





