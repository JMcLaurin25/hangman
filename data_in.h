#ifndef DATA_IN_H
#define  DATA_IN_H

int file_in(char *, char*);
void letter_in(int *, char *);
int menu_in(char *, int *, int *);
void stats_in(char *, int *);

#endif
