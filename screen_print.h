#ifndef SCREEN_PRINT_H
#define SCREEN_PRINT_H

void print_alphabet(int *);
void print_word(int *, char *);
void print_game(int *, int *, char *, int *);
void print_lose(void);
void print_stats(int *);
void print_welcome(void);
void print_exit(void);
void print_win(char *);
void print_selection(char *);

#endif
