
#include <stdio.h>
#include <string.h>
#include "screen_print.h"

void check_answer(char *answer, int *mask_word, char guess, int *counts) //Answer validation
{
	int index, length = strlen(answer), found = 0;

	for (index = 0; index < length; index++) {
		if ((answer[index] == guess) && (mask_word[index] == 1)) { //Match found, but already masked
			counts[1]++;
			found++;
			break;
		}

		if ((answer[index] == guess) || (answer[index] + ('a' - 'A') == guess)) { //MATCH FOUND
			mask_word[index] = 1;
			found++;
		} else if (!(answer[index] >= 'a' && answer[index] <= 'z') && !(answer[index] >= 'A' && answer[index] <= 'Z')) {
			mask_word[index] = 1;
		}
	}

	if (!found) {
		counts[1]++;
	}

	counts[0]++;
}

int win_lose(int *mask_word, char *answer)//Need to take in a count of wrongs.
{
	int index, count = 0, length = strlen(answer);

	for (index = 0; index < length; index++) {
		if (mask_word[index] == 1) { //If all characters masked
			count++;
		} 
	}
	if (count == length) {
		return 2;
	}

	return 0;
}

void mask_reset(int *mask) //Set alphabet mask to {0}
{
	int index;
	for (index = 'a'; index <= 'z'; index++) {
		mask[index - 'a'] = 0;
	}
}

void set_zero(char *word, int *word_mask) //Set new word mask to {0}
{
	int index, length = strlen(word);
	for (index = 0; index < length; index++) {
		*(word_mask + index) = 0;
	}
}

void store_stats(int *counts, int *stats, int win)
{
	if (win == 0) { //Game won
		stats[1]++;//Win Total
		stats[3]+= counts[0];//Total guesses, if won
		stats[4]+= counts[1];
	} else if (win == 1) { //Lost
		stats[2]++;//Loss total
	}

	stats[0]++;//Games played
	stats[5]+= counts[0];//Total guesses

}



